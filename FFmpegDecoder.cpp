#include "FFmpegDecoder.h"

namespace RTSPSERVER
{
    FFmpegDecoder::FFmpegDecoder(std::string path)
        : m_path(path)
        , m_codec_ctx(NULL)
        , m_fmt_ctx(NULL)
        , m_sws_ctx(NULL)
        , m_video_index(-1)
        , m_yuv_frame(NULL)
        , m_rgb_frame(NULL)
        , m_packet(NULL)
    {
    }

    bool FFmpegDecoder::intialize()
    {
        avformat_network_init();

        const AVCodec *v_codec = NULL;
        AVStream *video_st = NULL;

        int ret = avformat_open_input(&m_fmt_ctx, m_path.c_str(), NULL, NULL);
        if (ret < 0) {
            std::cerr << "avformat_open_input error: " << ret << std::endl;
            return false;
        }

        ret = avformat_find_stream_info(m_fmt_ctx, NULL);
        if (ret < 0) {
            std::cerr << "avformat_find_stream_info error: " << ret << std::endl;
            return false;
        }
        av_dump_format(m_fmt_ctx, 0, m_path.c_str(), 0);

        ret = av_find_best_stream(m_fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);
        if (ret < 0) {
            std::cerr << "av_find_best_stream video error: " << ret << std::endl;
            return false;
        }
        m_video_index = ret;
        video_st  = m_fmt_ctx->streams[m_video_index];
        int width  = video_st->codecpar->width;
        int height = video_st->codecpar->height;

        v_codec = avcodec_find_decoder(video_st->codecpar->codec_id);
        if (v_codec == NULL) {
            std::cerr << "avcodec_find_decoder error: " << ret << " video codec: " << avcodec_get_name(video_st->codecpar->codec_id) << std::endl;
            return false;
        }

        m_codec_ctx = avcodec_alloc_context3(v_codec);
        if (!m_codec_ctx) {
            std::cerr << "avcodec_alloc_context3 error: " << ret << std::endl;
            return false;
        }

        m_codec_ctx->extradata_size = video_st->codecpar->extradata_size + AV_INPUT_BUFFER_PADDING_SIZE;
        m_codec_ctx->extradata = (uint8_t *)av_mallocz(m_codec_ctx->extradata_size);
        memcpy(m_codec_ctx->extradata, video_st->codecpar->extradata, video_st->codecpar->extradata_size);

        if (avcodec_open2(m_codec_ctx, v_codec, NULL) < 0) {
            std::cerr << "avcodec_open2 error: " << ret << std::endl;
            return false;
        }

        m_rgb_frame = av_frame_alloc();
        AVPixelFormat dst_format = AV_PIX_FMT_BGR24;
        uint8_t *buffer = NULL;
        int buffer_size = 0;
        buffer_size = av_image_get_buffer_size(dst_format, width, height, 24);
        if (buffer_size < 0) {
            std::cerr << "av_image_get_buffer_size error: " << buffer_size << std::endl;
            return false;
        }
        buffer = (uint8_t *)av_malloc(buffer_size * sizeof(uint8_t));
        ret = av_image_fill_arrays(m_rgb_frame->data, m_rgb_frame->linesize, buffer, dst_format, width, height, 24);
        if (ret < 0) {
            std::cerr << "av_image_fill_arrays error: " << buffer_size << std::endl;
            return false;
        }

        m_sws_ctx = sws_getContext(width, height, (AVPixelFormat)video_st->codecpar->format, 
            width, height, dst_format, SWS_BICUBIC, NULL, NULL, NULL);
        if (!m_sws_ctx) {
            std::cerr << "sws_getCachedContext error " << std::endl;
            return false;
        }

        m_dec_params.dec_width   = width;
        m_dec_params.dec_height  = height;
        m_dec_params.dec_bitrate = (int)video_st->codecpar->bit_rate;
        m_dec_params.dec_gop     = m_codec_ctx->gop_size;
        m_dec_params.dec_frame_rate_num = video_st->avg_frame_rate.num;
        m_dec_params.dec_frame_rate_den = video_st->avg_frame_rate.den;

        m_yuv_frame = av_frame_alloc();
        m_packet    = av_packet_alloc();

        std::cerr << "[FFmpegDecoder::intialize] sucess" << std::endl;
        return true;
    }

    void FFmpegDecoder::setOnframeCallbackFunction(std::function<void(uint8_t *)> func)
    {
        m_on_frame = func;
    }

    void FFmpegDecoder::playMedia()
    {
        int ret = 0;
        bool got_frame = false;

        double sleep_time = 40000; // 40ms
        if (m_dec_params.dec_frame_rate_den) {
            double frame_rate = m_dec_params.dec_frame_rate_num / m_dec_params.dec_frame_rate_den;
            if (frame_rate > 0.0) {
                sleep_time = 1000000.0 / frame_rate;
            }
        }

        while ((av_read_frame(m_fmt_ctx, m_packet) >= 0)) {
            if (m_packet->buf && m_packet->stream_index == m_video_index) {
                got_frame = false;
                // submit the packet to the decoder
                ret = avcodec_send_packet(m_codec_ctx, m_packet);
                if (ret < 0) {
                    char buf[256] = {0};
                    av_strerror(ret, buf, 255);
                    std::cerr << "avcodec_send_packet error : " << buf << std::endl;
                    break;
                }

                // get all the available frames from the decoder
                while (ret >= 0) {
                    ret = avcodec_receive_frame(m_codec_ctx, m_yuv_frame);
                    if (ret < 0) {
                        break;
                    }

                    sws_scale(m_sws_ctx, m_yuv_frame->data, m_yuv_frame->linesize, 0, m_codec_ctx->height, m_rgb_frame->data, m_rgb_frame->linesize);
                    m_on_frame(m_rgb_frame->data[0]);
                    got_frame = true;
                    av_frame_unref(m_yuv_frame);
                }

                if (ret < 0) {
                    if (ret != AVERROR(EAGAIN)) {
                        char buf[256] = {0};
                        av_strerror(ret, buf, 255);
                        std::cerr << "avcodec_receive_frame error : " << buf << std::endl;
                        break;
                    }
                }
                av_packet_unref(m_packet);

                //for ondemand file
                if (!(m_fmt_ctx->iformat->flags & AVFMT_NOFILE))
                    std::this_thread::sleep_for(std::chrono::microseconds((long long)sleep_time));
            }
        }
        std::cerr << "[FFmpegDecoder::playMedia] done..." << std::endl;
    }

    void FFmpegDecoder::finalize()
    {
        sws_freeContext(m_sws_ctx);
        avformat_close_input(&m_fmt_ctx);
        avcodec_free_context(&m_codec_ctx);

        av_frame_free(&m_rgb_frame);
        av_frame_free(&m_yuv_frame);
        av_packet_free(&m_packet);
    }
}
