#ifndef _FFMPEG_DECODER_H_
#define _FFMPEG_DECODER_H_

#include <iostream>
#include <string>
#include <functional>
#include <chrono>
#include <thread>
#include <fstream>

extern "C" {
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

namespace RTSPSERVER
{
    struct DecParams {
        int dec_width;
        int dec_height;
        int dec_frame_rate_num;
        int dec_frame_rate_den;
        int dec_gop;
        int dec_bitrate; // bps
    };

    class FFmpegDecoder
    {
    public:
        FFmpegDecoder(std::string path);

        ~FFmpegDecoder() {};

        bool intialize();

        void playMedia(); // run on main thread

        void finalize();

        void setOnframeCallbackFunction(std::function<void(uint8_t *)> func);

    public:
        DecParams m_dec_params;
        std::function<void(uint8_t *)> m_on_frame; // callback when decode a frame

    private:
        std::string m_path;
        AVCodecContext  *m_codec_ctx;
        AVFormatContext *m_fmt_ctx;
        struct SwsContext *m_sws_ctx;

        int m_video_index;
        AVFrame *m_yuv_frame;
        AVFrame *m_rgb_frame;
        AVPacket *m_packet;
    };
}

#endif