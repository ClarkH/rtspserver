#include "FFmpegH264Encoder.h"

#define MAX_FRAME_NUM 30

namespace RTSPSERVER
{
    FFmpegH264Encoder::FFmpegH264Encoder()
        : m_codec(NULL)
        , m_codec_ctx(NULL)
        , m_src_picture(NULL)
        , m_dst_picture(NULL)
        , sws_ctx(NULL)
        , m_buffer_size(0)
        , m_drop_bs_cnt(0)
    {
    }

    void FFmpegH264Encoder::setCallbackFunctionFrameIsReady(std::function<void()> func)
    {
        m_on_packet = func;
    }

    void FFmpegH264Encoder::SendNewFrame(uint8_t *RGBFrame) {
        uint8_t *p = new uint8_t[m_buffer_size];
        memcpy(p, RGBFrame, m_buffer_size);

        std::unique_lock<std::mutex> lock(m_in_mutex);

        if (m_in_queue.size() < MAX_FRAME_NUM) {
            m_in_queue.push(p);
        } else {
            uint8_t *oldest_frame = m_in_queue.front();
            m_in_queue.pop();
            delete[] oldest_frame;

            m_in_queue.push(p);
            std::cerr << "drop yuv" << std::endl;
        }
        m_in_cv.notify_one();
    }

    void FFmpegH264Encoder::run()
    {
        while (true) {
            std::unique_lock<std::mutex> lock(m_in_mutex);
            while (m_in_queue.empty())
                m_in_cv.wait(lock);

            uint8_t *RGBFrame;
            RGBFrame = m_in_queue.front();
            m_in_queue.pop();
            lock.unlock();

            if (RGBFrame != NULL) {
                WriteFrame(RGBFrame);
                delete[] RGBFrame;
            }
        }
    }

    bool FFmpegH264Encoder::SetupCodec(AVCodecID codec_id)
    {
        int ret;
        m_sws_flags = SWS_BICUBIC;
        m_frame_count = 0;

        m_codec = (AVCodec *)avcodec_find_encoder(codec_id);
        if (!m_codec) {
            std::cerr << "avcodec_find_encoder error not found: " << avcodec_get_name(codec_id) << std::endl;
            return false;
        }

        int src_width = m_enc_params.src_width;
        int src_height = m_enc_params.src_height;

        int dst_width = m_enc_params.enc_width;
        int dst_height = m_enc_params.enc_height;

        m_codec_ctx = avcodec_alloc_context3(m_codec);
        m_codec_ctx->codec_id = codec_id;
        m_codec_ctx->bit_rate = m_enc_params.enc_bitrate;           //Bits Per Second 
        m_codec_ctx->width = dst_width;            //Note Resolution must be a multiple of 2!!
        m_codec_ctx->height = dst_height;          //Note Resolution must be a multiple of 2!!
        m_codec_ctx->time_base.den = m_enc_params.enc_frame_rate_den;      //Frames per second
        m_codec_ctx->time_base.num = m_enc_params.enc_frame_rate_den;
        m_codec_ctx->gop_size = m_enc_params.enc_gop;           // Intra frames per x P frames
        m_codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;      //Do not change this, H264 needs YUV format not RGB

        ret = avcodec_open2(m_codec_ctx, m_codec, NULL);
        if (ret < 0) {
            std::cerr << "avcodec_open2 error: " << ret << std::endl;
            return false;
        }

        m_dst_picture = av_frame_alloc();
        m_dst_picture->format = m_codec_ctx->pix_fmt;
        m_dst_picture->width = dst_width;
        m_dst_picture->height = dst_height;
        ret = av_image_alloc(m_dst_picture->data, m_dst_picture->linesize, m_dst_picture->width, m_dst_picture->height, (AVPixelFormat)m_dst_picture->format, 32);
        if (ret < 0) {
            std::cerr << "av_image_alloc error: " << ret << std::endl;
            return false;
        }
        m_dst_picture->pts = 0;

        m_src_picture = av_frame_alloc();
        m_src_picture->format = AV_PIX_FMT_BGR24;
        m_src_picture->width = src_width;
        m_src_picture->height = src_height;
        ret = av_image_alloc(m_src_picture->data, m_src_picture->linesize, src_width, src_height, AV_PIX_FMT_BGR24, 24);
        if (ret < 0) {
            std::cerr << "av_image_alloc error: " << ret << std::endl;
            return false;
        }
        m_buffer_size = ret;

        sws_ctx = sws_getContext(src_width, src_height, AV_PIX_FMT_BGR24,
            dst_width, dst_height, AV_PIX_FMT_YUV420P,
            SWS_BICUBIC, NULL, NULL, NULL);
        if (!sws_ctx) {
            std::cerr << "sws_getContext error: " << std::endl;
            return false;;
        }
        return true;
    }

    void FFmpegH264Encoder::WriteFrame(uint8_t *RGBFrame)
    {
        av_image_fill_arrays(m_src_picture->data, m_src_picture->linesize, RGBFrame,
            (AVPixelFormat)m_src_picture->format, m_src_picture->width, m_src_picture->height, 24);

        sws_scale(sws_ctx, m_src_picture->data, m_src_picture->linesize, 0, m_enc_params.src_height, m_dst_picture->data, m_dst_picture->linesize);

        AVPacket pkt = { 0 };
        av_init_packet(&pkt);

        int ret = avcodec_send_frame(m_codec_ctx, m_dst_picture);
        if (ret < 0) {
            fprintf(stderr, "Error sending a frame for encoding\n");
            return;
        }
        m_dst_picture->pts += 1;

        while (ret >= 0) {
            ret = avcodec_receive_packet(m_codec_ctx, &pkt);
            if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
                return;
            else if (ret < 0) {
                fprintf(stderr, "Error during encoding\n");
                return;
            }
            PacketData *pkt_data = new PacketData();
            pkt_data->dataPointer = new uint8_t[pkt.size];
            pkt_data->dataSize = pkt.size - 4;
            pkt_data->packetID = m_frame_count++;
            memcpy(pkt_data->dataPointer, pkt.data + 4, pkt.size - 4);

            std::unique_lock<std::mutex> lock(m_out_mutex);
            if (m_out_queue.size() < MAX_FRAME_NUM) {
                m_out_queue.push(pkt_data);
            } else {
                PacketData *oldest = m_out_queue.front();
                m_out_queue.pop();
                delete oldest;
                m_out_queue.push(pkt_data);
                //std::cerr << "drop bs: " << m_drop_bs_cnt++ << std::endl;
            }
            lock.unlock();

            av_packet_unref(&pkt);
            m_on_packet();
        }
    }

    void FFmpegH264Encoder::SetupVideo(EncParams enc_params)
    {
        m_enc_params = enc_params;
        SetupCodec(AV_CODEC_ID_H264);
    }

    void FFmpegH264Encoder::CloseCodec()
    {
        avcodec_free_context(&m_codec_ctx);
        av_frame_free(&m_src_picture);
        av_frame_free(&m_dst_picture);
        sws_freeContext(sws_ctx);
    }

    void FFmpegH264Encoder::CloseVideo()
    {
        CloseCodec();
    }

    bool FFmpegH264Encoder::GetFrame(uint8_t** FrameBuffer, unsigned int *FrameSize)
    {
        std::unique_lock<std::mutex> lock(m_out_mutex);
        if (!m_out_queue.empty()) {
            PacketData *pkt_data = m_out_queue.front();
            *FrameBuffer = (uint8_t*)pkt_data->dataPointer;
            *FrameSize = pkt_data->dataSize;
            return true;
        } else {
            *FrameBuffer = 0;
            *FrameSize = 0;
            return false;
        }
    }

    bool FFmpegH264Encoder::ReleaseFrame()
    {
        std::unique_lock<std::mutex> lock(m_out_mutex);
        if (!m_out_queue.empty()) {
            PacketData *pkt_data = m_out_queue.front();
            m_out_queue.pop();
            delete pkt_data;
        }
        return 1;
    }
}