#ifndef _FFMPEGH264_ENCODER_H
#define _FFMPEGH264_ENCODER_H

#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <iostream>
#include <functional>

extern "C" {
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
}

namespace RTSPSERVER
{
    class PacketData {
    public:
        PacketData() 
            : dataPointer(nullptr)
            , dataSize(0)
            , packetID(0) {
        }

        ~PacketData() {
            delete dataPointer;
        }
    public:
        uint8_t *dataPointer;
        int dataSize;
        int packetID;
    };

    struct EncParams {
        int src_width;
        int src_height;

        int enc_width;
        int enc_height;
        int enc_frame_rate_num;
        int enc_frame_rate_den;
        int enc_gop;
        int enc_bitrate; // bps
    };

    class FFmpegH264Encoder
    {
    public:
        FFmpegH264Encoder();
        ~FFmpegH264Encoder() {};

        void setCallbackFunctionFrameIsReady(std::function<void()> func);

        void SetupVideo(EncParams enc_params);
        void CloseVideo();
        bool SetupCodec(AVCodecID codec_id);
        void CloseCodec();

        void SendNewFrame(uint8_t * RGBFrame);
        void WriteFrame(uint8_t * RGBFrame);
        bool ReleaseFrame();

        void run();
        bool GetFrame(uint8_t** FrameBuffer, unsigned int *FrameSize);

    private:
        std::queue<uint8_t*> m_in_queue;
        std::mutex m_in_mutex;
        std::condition_variable m_in_cv;

        std::queue<PacketData *> m_out_queue;
        std::mutex m_out_mutex;

        int m_sws_flags;
        int m_frame_count;
        EncParams m_enc_params;

        AVCodec *m_codec;
        AVCodecContext *m_codec_ctx;
        AVFrame *m_src_picture;
        AVFrame *m_dst_picture;
        SwsContext *sws_ctx;
        int m_buffer_size;

        std::function<void()> m_on_packet; // trigger event when encoded a packet

        uint64_t m_drop_bs_cnt;
    };
}
#endif