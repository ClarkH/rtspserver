#include "FFmpegH264Source.h"

#include <time.h>
#ifdef WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif
#ifdef WIN32
int gettimeofday(struct timeval *tp, void *tzp)
{
    time_t clock;
    struct tm tm;
    SYSTEMTIME wtm;

    GetLocalTime(&wtm);
    tm.tm_year = wtm.wYear - 1900;
    tm.tm_mon = wtm.wMonth - 1;
    tm.tm_mday = wtm.wDay;
    tm.tm_hour = wtm.wHour;
    tm.tm_min = wtm.wMinute;
    tm.tm_sec = wtm.wSecond;
    tm.tm_isdst = -1;

    clock = mktime(&tm);
    tp->tv_sec = clock;
    tp->tv_usec = wtm.wMilliseconds * 1000;
    return (0);
}
#endif

namespace RTSPSERVER
{
    FFmpegH264Source *FFmpegH264Source::createNew(UsageEnvironment &env, FFmpegH264Encoder *source) {
        return new FFmpegH264Source(env, source);
    }

    FFmpegH264Source::FFmpegH264Source(UsageEnvironment &env, FFmpegH264Encoder *source) 
        : FramedSource(env)
        , m_source(source)
    {
        m_eventTriggerId = envir().taskScheduler().createEventTrigger(FFmpegH264Source::deliverFrameStub);
        std::function<void()> callback = std::bind(&FFmpegH264Source::onFrame, this);
        m_source->setCallbackFunctionFrameIsReady(callback);
    }

    FFmpegH264Source::~FFmpegH264Source()
    {
    }

    void FFmpegH264Source::doStopGettingFrames()
    {
        FramedSource::doStopGettingFrames();
    }

    void FFmpegH264Source::onFrame()
    {
        envir().taskScheduler().triggerEvent(m_eventTriggerId, this);
    }

    void FFmpegH264Source::doGetNextFrame()
    {
        deliverFrame();
    }

    void FFmpegH264Source::deliverFrame()
    {
        if (!isCurrentlyAwaitingData()) return; // we're not ready for the data yet

        static uint8_t *newFrameDataStart;
        static unsigned newFrameSize = 0;

        /* get the data frame from the Encoding thread.. */
        if (m_source->GetFrame(&newFrameDataStart, &newFrameSize)) {
            if (newFrameDataStart != NULL) {
                /* This should never happen, but check anyway.. */
                if (newFrameSize > fMaxSize) {
                    fFrameSize = fMaxSize;
                    fNumTruncatedBytes = newFrameSize - fMaxSize;
                } else {
                    fFrameSize = newFrameSize;
                }

                gettimeofday(&fPresentationTime, NULL);
                memcpy(fTo, newFrameDataStart, fFrameSize);

                m_source->ReleaseFrame();
            } else {
                fFrameSize = 0;
                fTo = NULL;
                handleClosure(this);
            }
        } else {
            fFrameSize = 0;
        }

        if (fFrameSize > 0)
            FramedSource::afterGetting(this);
    }
}