#include "LiveRTSPServer.h"

namespace RTSPSERVER
{
    LiveRTSPServer::LiveRTSPServer(FFmpegH264Encoder *encoder, int port, int httpPort)
        : m_encoder(encoder)
        , m_port(port)
        , m_http_tunnel_port(httpPort)
    {
        m_quit = 0;
    }

    LiveRTSPServer::~LiveRTSPServer()
    {
    }

    void LiveRTSPServer::run()
    {
        TaskScheduler    *scheduler;
        UsageEnvironment *env;

        scheduler = BasicTaskScheduler::createNew();
        env = BasicUsageEnvironment::createNew(*scheduler);

        OutPacketBuffer::maxSize = 2000000;
        RTSPServer *rtspServer = RTSPServer::createNew(*env, m_port, NULL);

        if (rtspServer == NULL) {
            *env << "LIVE555: Failed to create RTSP server: %s\n", env->getResultMsg();
        } else {
            if (m_http_tunnel_port) {
                rtspServer->setUpTunnelingOverHTTP(m_http_tunnel_port);
            }

            char const *descriptionString = "RTSPSERVER Streaming Session";
            char const *stream = "test";
            char const *info = "simple test";

            FFmpegH264Source *source = FFmpegH264Source::createNew(*env, m_encoder);
            StreamReplicator *inputDevice = StreamReplicator::createNew(*env, source, false);

            //ServerMediaSession* sms = ServerMediaSession::createNew(*env, RTSP_Address, RTSP_Address, descriptionString);
            ServerMediaSession *sms = ServerMediaSession::createNew(*env, stream, info, descriptionString);
            sms->addSubsession(RTSPSERVER::LiveServerMediaSubsession::createNew(*env, inputDevice));
            rtspServer->addServerMediaSession(sms);

            char *url = rtspServer->rtspURL(sms);
            *env << "Play this stream using the URL \"" << url << "\"\n";
            delete[] url;

            //signal(SIGNIT,sighandler);
            env->taskScheduler().doEventLoop(&m_quit); // does not return

            Medium::close(rtspServer);
            Medium::close(inputDevice);
        }

        env->reclaim();
        delete scheduler;
    }
}