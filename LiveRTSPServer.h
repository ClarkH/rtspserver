#ifndef _LIVE_RTSP_SERVER_H_
#define _LIVE_RTSP_SERVER_H_

#include <UsageEnvironment.hh>
#include <BasicUsageEnvironment.hh>
#include <GroupsockHelper.hh>
#include <liveMedia.hh>
#include "LiveServerMediaSubsession.h"
#include "FFmpegH264Source.h"
#include "FFmpegH264Encoder.h"

namespace RTSPSERVER {

    class LiveRTSPServer
    {
    public:
        LiveRTSPServer(FFmpegH264Encoder *encoder, int port, int httpPort);
        ~LiveRTSPServer();
        void run();

    private:
        int m_port;
        int m_http_tunnel_port;
        FFmpegH264Encoder * m_encoder;
        char m_quit;
    };
}
#endif