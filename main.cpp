#include "LiveRTSPServer.h"
#include "FFmpegH264Encoder.h"
#include "FFmpegDecoder.h"

RTSPSERVER::FFmpegH264Encoder *encoder;
RTSPSERVER::LiveRTSPServer *server;
RTSPSERVER::FFmpegDecoder *decoder;

int UDPPort;
int HTTPTunnelPort;
std::thread server_thread;
std::thread encoder_thread;

void *runServer(void *server)
{
    ((RTSPSERVER::LiveRTSPServer *)server)->run();
    return nullptr;
}

void *runEncoder(void *encoder)
{
    ((RTSPSERVER::FFmpegH264Encoder *)encoder)->run();
    return nullptr;
}

void onFrame(uint8_t *data)
{
    encoder->SendNewFrame(data);
}

int main(int argc, const char *argv[])
{
    if (argc >= 2)
        decoder = new RTSPSERVER::FFmpegDecoder(argv[1]);
    if (argc >= 3)
        UDPPort = atoi(argv[2]);
    if (argc >= 4)
        HTTPTunnelPort = atoi(argv[3]);

    decoder->intialize();
    decoder->setOnframeCallbackFunction(onFrame);

    RTSPSERVER::EncParams enc_params;
    enc_params.src_width = decoder->m_dec_params.dec_width;
    enc_params.src_height = decoder->m_dec_params.dec_height;
    enc_params.enc_bitrate = 100 * 1024;
    enc_params.enc_frame_rate_num = decoder->m_dec_params.dec_frame_rate_num;
    enc_params.enc_frame_rate_den = decoder->m_dec_params.dec_frame_rate_den;
    enc_params.enc_gop = 25;
    enc_params.enc_height = 360;
    enc_params.enc_width = 640;

    encoder = new RTSPSERVER::FFmpegH264Encoder();
    encoder->SetupVideo(enc_params);
    server = new RTSPSERVER::LiveRTSPServer(encoder, UDPPort, HTTPTunnelPort);

    server_thread = std::thread(runServer, server);
    server_thread.detach();

    encoder_thread = std::thread(runEncoder, encoder);
    encoder_thread.detach();

    // Play Media Here
    decoder->playMedia();
    decoder->finalize();
}
